import React, { Component } from 'react';
import { Typography, Grid } from '@material-ui/core';

class Round extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Grid>
          <Typography>{this.props.activityName}</Typography>
        </Grid>
        <Grid>
          <Typography>{this.props.round}</Typography>
        </Grid>
      </div>
    );
  }
}

export default Round;