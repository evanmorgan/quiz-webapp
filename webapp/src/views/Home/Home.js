import React, { Component } from 'react';
import { Typography, Grid, Button, Paper } from '@material-ui/core';
import { getSpec } from '../../utils/basic-api';
import ActivityButtonList from '../../components/ActivityButtonList/ActivityButtonList';
import Loading from '../../components/Loading/Loading';
import Round from '../Round/Round';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      name: 'defaultAppName',
      heading: 'defaultHeading',
      activities: [],
      homeScreen: true,
      quizScreen: false,
      scoreScreen: false,
      roundScreen: false,
      roundAlreadyDisplayed: false,
      currentActivity: [],
      recordedAnswers: [],
      currentOrder: 0,
      currentRoundIndex: 0,
      currentQuestionIndex: 0,
      roundsLeft: 1,
    };
  };

  startLoading = () => {
    this.setState({
      isLoading: true
    });
  };

  finishLoading = () => {
    this.setState({
      isLoading: false
    });
  };

  componentDidMount = () => {
    this.startLoading();
    getSpec().then((response) => {
      // Convert the buffer data to the app specification
      const appSpec = JSON.parse(Buffer.from(response.body).toString());
      this.setState({
        name: appSpec.name,
        heading: appSpec.heading,
        activities: appSpec.activities
      });
    }).catch((error) => {
      console.error(error);
      this.setState({ isLoading: false, name: "error", heading: "error", activities: [] });
    });
    this.finishLoading();
  };

  screenTransition = (target) => {
    this.startLoading();
    switch(target) {
      case 'home':
        this.setState({
          homeScreen: true,
          quizScreen: false,
          scoreScreen: false,
          roundScreen: false,
        });
        break;
      case 'quiz':
        this.setState({
          homeScreen: false,
          quizScreen: true,
          scoreScreen: false,
          roundScreen: false,
        });
        break;
      case 'score':
        this.setState({
          homeScreen: false,
          quizScreen: false,
          scoreScreen: true,
          roundScreen: false,
        });
        break;
      case 'round':
        this.setState({
          homeScreen: false,
          quizScreen: false,
          scoreScreen: false,
          roundScreen: true,
        });
        break;
      default:
        console.error('Incorrect screen transition detected');
    }
    this.finishLoading();
  };

  displayRoundScreen = () => {
    this.screenTransition('round');
    let timer = setInterval(() => {
      this.screenTransition('quiz');
      clearInterval(timer);
    }, 4000);
  };

  activityHandler = (activity) => {
    const currentOrder = activity.order;
    const roundsLeft = currentOrder === 1 ? 1 : activity.questions.length - 1 ;

    this.setState({
      currentActivity: activity,
      currentOrder,
      roundsLeft
    });
    this.screenTransition('quiz');
  };

  homeScreen = () => {
    return(
      <Grid container direction='column' alignItems='center'>
        <Grid item>
          <Typography>CAE</Typography>
        </Grid>
        <Grid item>
          <Typography variant='h1'>{this.state.name}</Typography>
        </Grid>
        <Grid item>
          <ActivityButtonList
            activities={this.state.activities}
            activityHandler={this.activityHandler}
          />
        </Grid>
        <Grid item>
          <Button disabled>RESULTS</Button>
        </Grid>
      </Grid>
    );
  };

  handleSubmit = (answer) => {
    const recordedAnswers = this.state.recordedAnswers;
    const questionPool = this.state.currentOrder === 1 ?
      this.state.currentActivity.questions.length :
      this.state.currentActivity.questions[this.state.currentRoundIndex].questions.length
    const result = 
      this.state.currentActivity.questions[this.state.currentQuestionIndex].is_correct === answer ?
        "CORRECT" : "FALSE";
    recordedAnswers.push(result);

    // Keep recording answers
    if (recordedAnswers.length < this.state.currentActivity.questions.length) {
      this.setState({
        recordedAnswers,
        currentQuestionIndex: this.state.currentQuestionIndex + 1
      });
    } else {
      this.setState({
        recordedAnswers,
        currentQuestionIndex: 0,
        roundsLeft: this.state.roundsLeft - 1,
        roundAlreadyDisplayed: false
      });

      if (this.state.roundsLeft <= 0) {
        this.screenTransition('score');
      } else {
        this.setState({currentRoundIndex: this.state.currentRoundIndex + 1});
      }
    }
  };

  quizScreen = (activity, order) => {
    let questionNumber = this.state.currentQuestionIndex + 1;
    let currentStimulus = 
      activity.questions[this.state.currentQuestionIndex].stimulus ? 
      activity.questions[this.state.currentQuestionIndex].stimulus : 
      activity.questions[this.state.currentRoundIndex].questions[this.state.currentQuestionIndex].stimulus ;

    if (order === 2 &&
      this.state.currentQuestionIndex === 0 &&
      !this.state.roundScreen &&
      !this.state.roundAlreadyDisplayed) {
      this.displayRoundScreen();
      this.setState({
        roundAlreadyDisplayed: true
      });
    } else if (order === 1) {
      return (
        <Grid container direction='column'>
          <Grid item>
            <Typography>
              {activity.activity_name.toUpperCase()} / ROUND 1
            </Typography>
          </Grid>
          <Grid item>
            <Typography>Q{questionNumber}</Typography>
          </Grid>
          <Grid item>
            <Typography>{currentStimulus}</Typography>
          </Grid>
          <Grid container item direction='row' alignItems='center'>
            <Grid item>
              <Button onClick={() => this.handleSubmit(true)}>Correct</Button>
            </Grid>
            <Grid item>
              <Button onClick={() => this.handleSubmit(false)}>Incorrect</Button>
            </Grid>
          </Grid>
        </Grid>
      );
    } else if (order === 2) {
      return (
        <Grid container direction='column'>
          <Grid item>
            <Typography>
              {activity.activity_name.toUpperCase()} / {this.state.currentActivity.questions[this.state.currentRoundIndex].round_title.toUpperCase()}
            </Typography>
          </Grid>
          <Grid item>
            <Typography>Q{questionNumber}</Typography>
          </Grid>
          <Grid item>
            <Typography>{currentStimulus}</Typography>
          </Grid>
          <Grid container item direction='row' alignItems='center'>
            <Grid item>
              <Button onClick={() => this.handleSubmit(true)}>Correct</Button>
            </Grid>
            <Grid item>
              <Button onClick={() => this.handleSubmit(false)}>Incorrect</Button>
            </Grid>
          </Grid>
        </Grid>
      );
    }
  }

  scoreScreen = (activity, recordedAnswers) => {
    const tableContents = {};
    for (let i = 0; i < recordedAnswers.length; i++) {
      tableContents[i] = recordedAnswers[i];
    };

    return(
      <Grid container direction='column' alignItems='center'>
        <Grid item>
          <Typography>{activity.activity_name.toUpperCase()}</Typography>
        </Grid>
        <Grid item>
          <Typography variant='h1'>Results</Typography>
        </Grid>
        <Grid item>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableBody>
                {Object.keys(tableContents).map((answerKey) => (
                  <TableRow key={answerKey}>
                    <TableCell component="th" scope="row">
                      Q{parseInt(answerKey)+1}
                    </TableCell>
                    <TableCell align="right">{tableContents[answerKey]}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        <Grid item>
          <Button onClick={() => this.screenTransition('home')}>Home</Button>
        </Grid>
      </Grid>
    );
  };

  render() {
    if (this.state.isLoading){
      return (
        <div>
          <Loading/>
        </div>
      );
    } else if (this.state.homeScreen) {
      return (
        <div>
          {this.homeScreen()}
        </div>
      );
    } else if (this.state.quizScreen) {
      return (
        <div>
          {this.quizScreen(
            this.state.currentActivity,
            this.state.currentOrder
          )}
        </div>
      );
    } else if (this.state.scoreScreen) {
      return (
        <div>
          {this.scoreScreen(
            this.state.currentActivity,
            this.state.recordedAnswers
          )}
        </div>
      );
    } else if (this.state.roundScreen) {
      return (
        <Round
          activityName={this.state.currentActivity.activity_name}
          round={this.state.currentActivity.questions[this.state.currentRoundIndex].round_title}
        />
      );
    }
  }
}

export default Home;