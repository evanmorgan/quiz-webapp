import React, { Component } from 'react';
import { Typography, Grid, Button } from '@material-ui/core';
import { getSpec } from '../../utils/basic-api';
import { makeStyles } from '@material-ui/core/styles';
import ActivityButtonList from '../../components/ActivityButtonList/ActivityButtonList';

const useStyles = makeStyles((theme) => ({
  title: {
    color: '#2E9DFB'
  },
  writing: {
    color: '#2E9DFB'
  }
}));

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      name: 'defaultAppName',
      heading: 'defaultHeading',
      activities: []
    };
  };

  componentDidMount = () => {
    getSpec().then((response) => {
      // Convert the buffer data to the app specification
      const appSpec = JSON.parse(Buffer.from(response.body).toString());
      this.setState({ 
        isLoading: false,
        name: appSpec.name,
        heading: appSpec.heading,
        activities: appSpec.activities
      });
    }).catch((error) => {
      console.error(error);
      this.setState({ isLoading: false, name: "error", heading: "error", activities: [] });
    });
  };

  render() {
    const classes = useStyles();

    return (
      <div>
        <Grid container direction='column' alignItems='center'>
          <Grid item>
            <Typography className={classes.writing}>CAE</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.title} variant='h1'>{this.state.name}</Typography>
          </Grid>
          <Grid item>
            <ActivityButtonList activities={this.state.activities}/>
          </Grid>
          <Grid item>
            <Button disabled>RESULTS</Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default Home;