import React, { Component } from 'react';
import ActivityButton from '../ActivityButton/ActivityButton';
import { Grid } from '@material-ui/core';

class ActivityButtonList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (this.props.activities.length > 0 &&
      <div>
        <Grid container spacing={1} direction='column'>
          {this.props.activities.map(activity => (
              <Grid item>
                <ActivityButton
                  activity={activity}
                  activityHandler={this.props.activityHandler}
                />
              </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}

export default ActivityButtonList;