import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  spinner: {
    position: 'fixed',
    top: '50%',
    right: '50%',
    zIndex: 1500,
  },
}
));

function Loading() {
  const classes = useStyles();
  return (
    <CircularProgress className={classes.spinner} />
  );
}

export default Loading;
