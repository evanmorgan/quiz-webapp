import React, { Component } from 'react';
import { Button } from '@material-ui/core';

class ActivityButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.activity.activity_name
    };
  }
  
  render() {
    return (
      <div>
        <Button color="primary" onClick={() => this.props.activityHandler(this.props.activity)}>
          {this.state.name}
        </Button>
      </div>
    );
  }
}

export default ActivityButton;