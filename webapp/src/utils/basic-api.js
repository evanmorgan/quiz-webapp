export function getSpec() {
  const basicAPI = "https://5amn57rih3.execute-api.eu-west-1.amazonaws.com/demo";
  return fetch(`${basicAPI}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      })
    .then((response) => {
      if (response.status === 200) {
        return response.json();
      }
      throw Error(`${response.status} ${response.statusText}`);
    });
}