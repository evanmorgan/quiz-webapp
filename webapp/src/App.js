import React, { Suspense } from 'react';
import {
  BrowserRouter as Router, Route, Switch,
} from 'react-router-dom';
import './App.css';
import Loading from './components/Loading/Loading';

const Home = React.lazy(() => import('./views/Home/Home'));

function App() {
  return (
    <Router basename="app">
        <Suspense fallback={<Loading />}>
          <Switch>
            <Route exact path="/" name="home" component={Home} />
          </Switch>
        </Suspense>
    </Router>
  );
}

export default App;
