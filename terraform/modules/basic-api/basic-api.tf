resource "aws_api_gateway_rest_api" "basic-api" {
  name        = "${var.project_name}-basic-api-${var.environment}"
  description = "${var.project_name}-basic-api-${var.environment}"
  body = "${file("../../../swagger/demo.yaml")}"
}

resource "aws_api_gateway_deployment" "basic-api" {
  rest_api_id = "${aws_api_gateway_rest_api.basic-api.id}"
  stage_name  = "${var.environment}"

  stage_description = "${md5(file("../../../swagger/demo.yaml"))}"
  
  depends_on = ["aws_api_gateway_rest_api.basic-api"]

  lifecycle {
    create_before_destroy = true
  }
}

output "api_base_url" {
  value = "${aws_api_gateway_deployment.basic-api.invoke_url}"
}
