# Setup the bucket to store Lambda functions
resource "aws_s3_bucket" "lambda_store" {
  bucket = "${var.project_name}-${var.environment}-lambda-store"
  acl    = "private"

  tags = {
    Name        = "Lambda Store"
    Environment = var.environment
  }
}

# Setup the bucket to store Lambda functions
resource "aws_s3_bucket" "webapp_bucket" {
  bucket = "${var.project_name}-${var.environment}-webapp"
  acl    = "private"

  tags = {
    Name        = "Webapp Bucket"
    Environment = var.environment
  }
  
  website {                        
   index_document = "index.html"
  }
}