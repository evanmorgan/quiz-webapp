variable "environment" {
  default = "demo"
}

variable "project_name" {
  default = "quiz-webapp"
}
