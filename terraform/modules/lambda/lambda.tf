resource "aws_lambda_function" "s3-get-object" {
  function_name = "${var.project_name}-${var.environment}-s3-get-object"

  # The bucket name created by the initial infrastructure set up
  s3_bucket = "${var.project_name}-${var.environment}-lambda-store"
  s3_key    = "v1.0.0/s3-get-object.zip"

  # "index" is the filename within the zip file (index.js) and "handler" is the name of the
  # property under which the handler function was exported in that file.
  handler = "index.handler"
  runtime = "nodejs12.x"
  timeout = 10
  role = aws_iam_role.s3-get-object-role.arn

  environment {
    variables = {
      region =  "${var.region}",
      targetBucket = "${var.project_name}-${var.environment}-webapp"
      targetFile = "spec/payload.json"
    }
  }
}

# IAM role which dictates what other AWS services the Lambda function may access.
resource "aws_iam_role" "s3-get-object-role" {
  name = "${var.project_name}-${var.environment}-s3-get-object-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "s3-get-object-policy" {
    name        = "${var.project_name}-${var.environment}-s3-get-object-policy"
    description = ""
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    },
    {
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${var.project_name}-${var.environment}-webapp"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "s3-get-object-attach" {
    role       = aws_iam_role.s3-get-object-role.name
    policy_arn = aws_iam_policy.s3-get-object-policy.arn
}

resource "aws_lambda_permission" "s3-get-object-apigw" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3-get-object.arn
  principal     = "apigateway.amazonaws.com"
}
