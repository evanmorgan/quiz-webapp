variable "region" {
  default = "eu-west-1"
}

variable "environment" {
  default = "demo"
}

variable "project_name" {
  default = "quiz-webapp"
}

variable "account_id" {
  default = "711892051847"
}
