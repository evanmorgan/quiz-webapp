# Configure the S3 backend, which needs to be set up separately
terraform {
  backend "s3" {
    region         = "eu-west-1"
    bucket         = "quiz-webapp-demo-tf-state"
    key            = "quiz-webapp/terraform.tfstate"
    dynamodb_table = "quiz-webapp-demo-infra"
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.region
}

# Set up the basic API
module "basic-api" {
  source = "../../modules/basic-api"
  project_name = var.project_name
  environment = var.environment
}

# Set up the lambda store
module "s3" {
  source      = "../../modules/s3"
  environment = var.environment
  project_name = var.project_name
}

# Set up the lambda store
module "lambda" {
  source      = "../../modules/lambda"
  environment = var.environment
  project_name = var.project_name
}
