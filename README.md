# quiz-webapp
Demo quiz webapp which takes in a data payload from a defined REST API, technologies used are NodeJS, React, Terraform and Swagger. This data payload defines what questions the app asks, what are the correct answers and other information in the app.

For a mock wireframe of the finished article: https://xd.adobe.com/view/36feaa87-e6e3-4cc4-4acc-91842640b5b1-40cf/screen/41bdb932-efe1-43a1-a497-8343a6713cf7

Currently hosted at: http://quiz-webapp-demo-webapp.s3-website-eu-west-1.amazonaws.com/

## Management
There are still a few issues with the app left to manage. These tickets have been raised in the issues screen on Gitlab.

## Backend
### AWS
This project is held together by glue, magic and AWS. It uses API Gateway for a REST API that the client calls to retrieve the payload.json file which contains the app specification.

### Terraform
The backend was provisioned using aws resources with Terraform - you'll find the scripts in the terraform folder. For the sake of reusability they've been split into modules and infrastructure. The infrastructure folder is used to orchestrate the provisioning, while the modules should be easily reused across projects.

### Swagger
A swagger file is used to define the REST API so that it is reproduceable and clearly defined. This is kept in the swagger folder.

### Lambda
To retrieve the payload specification file from S3, a lambda function is used and the code for that is stored in the lambda folder. AWS Lambda is a microservices platform offered by AWS.

## Frontend
The webapp is in the webapp folder, it was originally created with the create-react-app node package but it has gone long past that now. 

To run the app locally, navigate to the webapp folder and `npm install` to install dependencies. Once the dependencies are installed, you can `npm run start` to run it on localhost:3000.