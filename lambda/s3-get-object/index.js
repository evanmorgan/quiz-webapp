const AWS = require('aws-sdk');
const logger = require('pino')({ name: 'S3 Get Object', level: 'debug' });
AWS.config.update({ region: process.env.region });
const s3 = new AWS.S3();

exports.handler = async (event) => {
  logger.debug(event);

  const params = {
    Bucket: process.env.targetBucket, 
    Key: process.env.targetFile
  };

  const specPayload = await s3.getObject(params).promise();
  const response = {
    statusCode: 200,
    body: specPayload.Body,
  };

  return response;
};
